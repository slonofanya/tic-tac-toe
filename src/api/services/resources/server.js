'use strict'

require('./lib/globals')

const serverConfig = $require('lib/config')('server')
const App = $require('lib/initializers/app')

const startApp = new App()
  .config(serverConfig)
  .start()
  .tap(app => {
    app.loadResources('entities')
  })

module.exports = Promise.all([startApp])
  .tap(() => {
    console.log(`Server started at: ${serverConfig.hostname}:${serverConfig.port}`)
  })
