'use strict'

const path = require('path')
const Promise = require('bluebird')
const _ = require('lodash')

const appPath = process.cwd()

function resolvePath() {
  return path.resolve.apply(null, [appPath].concat(...arguments))
}

function $require(incPath) {
  return require(`${appPath}/${incPath}`)
}

_.mixin(require('lodash-inflection'))

Object.assign(global, {
  $require,
  Promise,
  resolvePath,
  appPath,
  _
})
