'use strict'

const _ = require('lodash')

const basePath = process.cwd()
const konphyg = require('konphyg')(`${basePath}/config`)
const prefix = 'WEB_APPS_API_METRICS'
let generated = {}

function getConfig(configName) {
  const constants = konphyg(_.kebabCase(configName))

  if (!constants) {
    return {}
  }

  return Object.keys(constants).reduce((result, key) => {
    if (typeof constants[key] === 'function') {
      return result
    }

    const envVarKey = `${prefix}_${_.snakeCase(configName).toUpperCase()}_${_.snakeCase(key).toUpperCase()}`

    result[key] = process.env[envVarKey] || constants[key] || ''

    return result
  }, {})
}

module.exports = configName => {
  if (!configName) {
    return generated
  }

  if (!generated[configName]) {
    generated[configName] = getConfig(configName)
  }

  return generated[configName]
}
