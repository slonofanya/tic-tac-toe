'use strict'

const fs = require('fs')
const jsonApi = require('jsonapi-server-sl-fork')

const resourceNamePattern = /^(?!.*?index).*.js$/m

function defineResource(app, pathToEntities, resourceName) {
  const resourceData = require(`${pathToEntities}/${_.kebabCase(resourceName)}`)

  jsonApi.define(_.merge({
      resource: _.camelCase(resourceName),
      handlers: app.handlers()
    },
    resourceData
  ))
}

module.exports = (app, entitiesDir) => {
  const pathToEntities = `${appPath}/${entitiesDir}`

  fs.readdirSync(pathToEntities)
    .filter(filename => resourceNamePattern.test(filename))
    .forEach(filename => defineResource(app, pathToEntities, filename.replace(/\.js$/, '')))
}
