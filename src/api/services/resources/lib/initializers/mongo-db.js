'use strict'

const mongoose = require('mongoose')

module.exports = config => new Promise((resolve, reject) => {
  const db = mongoose.connect(config.uri, err =>
    err ? reject(err) : resolve(db)
  )
})
