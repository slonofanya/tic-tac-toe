'use strict'

const debug = require('debug')('jsonapi-server-sl-fork')
const jsonApi = require('jsonapi-server-sl-fork')
const MongoStore = require('jsonapi-store-mongodb')
const mongoConfig = $require('lib/config')('mongoDb')
const loadResources = $require('lib/load-resources')

function applyProperty(property, data) {
  const propName = `propData${property}`

  if (data) {
    this[propName] = data
    return this
  }

  return this[propName]
}

class App {
  constructor() {
    this.server = jsonApi
  }

  config(config) {
    return applyProperty.call(this, 'config', config)
  }

  handlers() {
    // don't reuse connections!!!
    return new MongoStore(mongoConfig)
  }

  loadResources(pathToEntities) {
    loadResources(this, pathToEntities)
    return this
  }

  start() {
    this.server.setConfig(this.config())

    // this.server.authenticate(null)
    // this.server.authenticate(function(request, res, callback) {
    //   console.log(request)
    //   return callback()
    // })

    return new Promise(resolve => {
      this.server.start()
      resolve(this)
    })
        .catch(err => console.log(err))
  }
}

module.exports = App
