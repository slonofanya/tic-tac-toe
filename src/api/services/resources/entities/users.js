'use strict'

const jsonApi = require('jsonapi-server-sl-fork')
const joi = require('joi')

module.exports = {
  attributes: {
    code: jsonApi.Joi.string().required(),
    email: jsonApi.Joi.string().required(),
    password: jsonApi.Joi.string().required(),
    akavita_password: jsonApi.Joi.string(),
    domain: jsonApi.Joi.string(),
    name: jsonApi.Joi.string(),
    description: jsonApi.Joi.string(),
    status: jsonApi.Joi.string(),
    /*reviews: jsonApi.Joi.many('reviews'),*/
    reviews: jsonApi.Joi.belongsToMany({
      resource: "reviews",
      as: "owner"
    }),
    role: jsonApi.Joi.string().default('user'), //.allow(null).valid(null),
    slug: jsonApi.Joi.string(),
    editMode: jsonApi.Joi.string().default('simple'),

    gitlabUsername: jsonApi.Joi.string(),
    gitlabPassword: jsonApi.Joi.string(),

    createdAt: jsonApi.Joi.number().integer().default(Date.now, 'date, created'),
    confirmedAt: jsonApi.Joi.number().integer().default(0),
    blockedAt: jsonApi.Joi.number().integer(),
    updatedAt: jsonApi.Joi.number().integer(),
    developerAt: jsonApi.Joi.number().integer(),
    partnerAt: jsonApi.Joi.number().integer(),
    lastLogin: jsonApi.Joi.number().integer().default(0),
    partner: jsonApi.Joi.one('partners').allow('').default(''),
    partnerUserId: jsonApi.Joi.string().allow('').default(''),
    widgetSettings: jsonApi.Joi.object(),

  },
  examples: [
    {
      "email": "test-user@test.com",
      "password": "somehashofpassword",
      "confirmedAt": 100500,
      "blockedAt": 100500,
      "updatedAt": 100500
    }
  ]
}
