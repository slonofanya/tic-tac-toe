'use strict'

const http = require('http')
const express = require('express')

function applyProperty(property, data) {
  const propName = `propData${property}`

  if (data) {
    this[propName] = data
    return this
  }

  return this[propName]
}

class App {
  constructor() {
    this.express = express()
  }

  static getInstance() {
    return instance
  }

  config(config) {
    return applyProperty.call(this, 'config', config)
  }

  logger(logger) {
    return applyProperty.call(this, 'logger', logger)
  }

  applyMiddleWares(middleWareList) {
    _.forEach(middleWareList, this.applyMiddleWare.bind(this))
    return this
  }

  applyMiddleWare(middleWare) {
    middleWare(this.express)

    return this
  }

  services(services) {
    return applyProperty.call(this, 'services', services(this.express))
  }

  start() {
    const config = this.config()
    const app = this.express

    app.set('port', config.port || 3000)
    // app.use(this.logger().errorLogger())

    app.get('/', (req, res) => {
      res.json({'It': 'works'})
    })

    const server = new http.Server(app)

    return new Promise(resolve => {
      const instance = server.listen(app.get('port'), () => resolve(instance))
    })
  }
}

const instance = new App()

module.exports = App
