'use strict'

const url = require('url')
const querystring = require('querystring')
const urlencode = require('urlencode');
const proxyMiddleware = require('http-proxy-middleware')
const getConfig = $require('lib/config')
const _ = require('lodash')

const serverConfig = getConfig('server')
const serviceList = getConfig('services')

module.exports = app => {
  Object.keys(serviceList).forEach(i => {
    const service = serviceList[i]
    const servicePath = `/v${serverConfig.apiVersion}/${service.context}`
    const options = {
      target: `http://${service.host}:${service.port}`,
      changeOrigin: false,
      pathRewrite: { [`^${servicePath}`]: '' },
      onProxyReq: (proxyReq) => {

        proxyReq.path = removeApiKeyParam(proxyReq.path)
        proxyReq.setHeader('x-gateway-peroxided', true)
        proxyReq.setHeader('Accept-Encoding', 'none')

        var user = app.get('user')
        if (_.has(user, 'email'))  user.email = urlencode(user.email)
        if (_.has(user, 'name')) user.name = urlencode(user.name)

        proxyReq.setHeader('user', JSON.stringify(user) || '{}')
      },
      // onProxyRes: (proxyRes) => {
      //   console.log(proxyRes)
      // },
      xfwd: true
    }
    const proxy = proxyMiddleware(servicePath, options)

    app.use(proxy)
  })

  return app
}

function removeApiKeyParam(uri) {
  const parsedUri = url.parse(uri, true)
  parsedUri.query = _.omit(parsedUri.query, [serverConfig.apiKeyField])
  return `${parsedUri.pathname}?${querystring.stringify(parsedUri.query)}`
}
