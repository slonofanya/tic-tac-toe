'use strict'

const getConfig = $require('lib/config')
const bodyParser = $require('middlewares/body-parser')
const serverConfig = getConfig('server')
const servicesConfig = getConfig('services')
const passport = require('passport')
const superagent = require('superagent')
const LocalAPIKeyStrategy = require('passport-localapikey').Strategy
const LocalStrategy = require('passport-local').Strategy
const cookieParser = require('cookie-parser')
const session = require('express-session')
const unless = require('express-unless')
const moment = require('moment')

const redis = require('promise-redis')()
const redisConfig = $require('lib/config')('redis')
const redisBaseConfig = _.pick(redisConfig, ['host', 'port'])
const redisClient = redis.createClient(_.assign({}, redisBaseConfig, {
  db: redisConfig.dbs.token
}))
const userTokenRedisPrefix = 'user_token_'

const crypto = require('crypto')
const randtoken = require('rand-token').generator({
  chars: 'a-z',
  source: crypto.randomBytes
})
const strategyOptions = { session: false }
var maxAge = 3600000 * 24 // A day
var ttl = 3600000 * 24 * 30 // A month
const sessionOptions = {
  resave: true,
  saveUninitialized: false,
  secret: 'keyboard cat, generated via some super crypt method',
  maxAge,
  ttl
}

const basePath = `/v${serverConfig.apiVersion}`
const tokenPath = `${basePath}/token`
const logoutPath = `${basePath}/logout`
const authUserPath = `${basePath}/users/auth_user`
const usersPath = `${basePath}/resources/users`

const resourcesUrl = `http://${servicesConfig.resources.host}:${servicesConfig.resources.port}`
const apiUsersUrl = `${resourcesUrl}/users`

const apiKeyAuthMiddleware = passport.authenticate('localapikey', strategyOptions)
const credentialsAuthMiddleware = passport.authenticate('local', strategyOptions)

const authExcludePaths = {
  path: [
    '', '/',
    { url: startsFrom(usersPath), methods: ['POST', 'PATCH'] },
  ],
  useOriginalUrl: true
}

module.exports = app => {
  app.use(session(sessionOptions))
  bodyParser(app, [
    tokenPath,
    startsFrom(usersPath)
  ])

  app.post(usersPath, signUp)

  app.use(cookieParser())
  app.use(passport.initialize())

  app.post(tokenPath, credentialsAuthMiddleware, generateApiKey)
  app.get(authUserPath, apiKeyAuthMiddleware, getCurrentUser)
  app.all(logoutPath, apiKeyAuthMiddleware, deleteApiKey)

  app.use(apiKeyAuthMiddleware.unless(authExcludePaths), (req, res, next) => {
    req.session.cookie.expires = new Date(Date.now() + maxAge)
    app.set('user', req.user || null)
    next()
  })

  app.use((req, res, next) => {
    if (!app.get('user')) {
      const token = _.get(req, `query.${serverConfig.apiKeyField}`, '')

      return getUserByApiKey(token).then(user => {
        app.set('user', user)
      })
        .then(next)
    }

    next()
  })

  return app
}

passport.use(new LocalAPIKeyStrategy((apiKey, next) => {
  getUserByApiKey(apiKey)
    .then(redisUser => redisUser ? redisUser.email : '')
    .then(email => getUserByEmail(email))
    .then(user => {
      next(null, user)
    })
    .catch(() => {
      next(null, null)
    })
}))

passport.use(new LocalStrategy((login, password, next) => {
  getUserByCredentials(login, password)
    .then(user => {
      next(null, user)
    })
    .catch(() => {
      next(null, null)
    })
}))

apiKeyAuthMiddleware.unless = unless

function getUserByApiKey(token) {
  return redisClient.get(`${userTokenRedisPrefix}${token}`)
    .then(doc => JSON.parse(doc))
    .catch(() => null)
}

function getUserByCredentials(email, password) {
  const getUser = query => new Promise((resolve, reject) => {
    const userNotFoundError = new Error('Wrong login or password')

    if (!email || !password) {
      return reject(userNotFoundError)
    }

    superagent.get(apiDataSearchUsersUrl)
      .query(query)
      .send()
      .end((err, body) => {
        err ? reject(err) : resolve(body.body)
      })
  })

  return Promise.all([
    getUser({ email, password })
  ])
    .spread((simpleMethodRes) => {
      return simpleMethodRes
    })
}

function generateApiKey(req, res) {
  const token = randtoken.generate(128)
  const user = req.user
  const role = _.get(req, 'body.role')

  if (!user || !user.id) {
    return res.status(400).json({ error: 'Wrong login or password' })
  }

  if (!_.isEmpty(role)) {
    if (_.get(user, 'role')!=role) {
      return res.status(400).json({ error: 'Wrong permissions' })
    }
  }

  return redisClient.setex(`${userTokenRedisPrefix}${token}`, 604800, JSON.stringify(user)) //ttl week
    .then(() => {
      res.json({ token })
      res.end()
    })
      .catch(error => {
        res.status(400).json({ error })
      })
    .then(() => {
      // update user.lastLogin
      return proxyRequest('patch', `${apiUsersUrl}/${user.id}`, '', {data: {attributes: {lastLogin: moment().utc().valueOf()}}})
    })
    .catch(error => {
      res.status(400).json({ error })
    })
}

function proxyRequest(method, url, query, data) {
  return new Promise((resolve, reject) => {
    superagent[method](url)
      .query(query)
      .send(data)
      .end((error, body) => {
        error ? reject(new Error(_.get(error, 'response.text', ''))) : resolve(body.text)
      })
  })
}

function signUp(req, res) {
  superagent.post(apiUsersUrl)
    .query(req.query)
    .send(req.body)
    .end((error, body) => {
      if (error) {
        return res.status(400).json(JSON.parse(_.get(error, 'response.text', {})))
      }

      const text = JSON.parse(body.text)
      const user = _.get(text, 'data[0]', _.get(text, 'data', null))

      res.json(user)
    })
}

function getCurrentUser(req, res) {

    const token = _.get(req, `query.${serverConfig.apiKeyField}`)

    const user = _.omit(_.pick(req.user, serverConfig.allowedUserFields), ["gitlabUsername", "gitlabPassword"])
    redisClient.expire(`${userTokenRedisPrefix}${token}`, 604800) //ttl week
        .catch(error =>
          console.log(error)
        )

    return res.json(user)
}

function deleteApiKey(req, res) {
  const token = _.get(req, `query.${serverConfig.apiKeyField}`)

  if (!token) {
    res.status(401).json({})
  }

  return redisClient.del(`${userTokenRedisPrefix}${token}`)
    .then(() => {
      res.json({ logout: true })
    })
    .catch(error => {
      res.status(400).json({ error })
    })
}

