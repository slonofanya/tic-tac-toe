"use strict"

const bodyParser = require('body-parser')

module.exports = (express, paths) => {
  _.forEach(paths, path => {
    express.use(path, bodyParser.json())
    express.use(path, bodyParser.urlencoded({ extended: false }))
  })
}
