'use strict'

const httpProxy = require('http-proxy')
const getConfig = $require('lib/config')

const serverConfig = getConfig('server')
const serviceList = getConfig('services')


module.exports = app => {
  app.use((req, res, next) => {
    const replaceMatch = new RegExp(`\/?v${serverConfig.apiVersion}\/?`)
    const urlParts = req.url.replace(replaceMatch, '').split('/')
    const service = serviceList[urlParts.shift()]
    const targetPath = urlParts.join('/')
    const target = `http://${service.host}:${service.port}/${targetPath}`

    const proxy = httpProxy.createProxyServer({ target, ws: false })

    proxy.web(req, res, { target })
  })

  return app
}
