"use strict"

const superagent = require('superagent')
const basePath = 'http://127.0.0.1:30041'

module.exports = (req, res) => {
  const method = req.method.toLowerCase()
  const url = `${basePath}${req.originalUrl}`
  const request = superagent[method](url)
  const params = null
  const data = null

  if (params) {
    request.query(params)
  }

  if (data) {
    request.send(data)
  }

  request.end((err, body) => {
    return res.json(body ? JSON.parse(body.text) : err)
  })
}
