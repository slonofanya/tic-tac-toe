"use strict"

const bodyParser = require('body-parser')

module.exports = (express) => {
  express.use(bodyParser.json())
  express.use(bodyParser.urlencoded({ extended: false }))
}
