"use strict"

const superagent = require('superagent')
const config = $require('lib/config')('server')
const configResources = $require('lib/config')('middle-services').resources

const baseUrl = `http://${configResources.host}:${configResources.port}`

module.exports = (req, res) => {
  const method = req.method.toLowerCase()
  const url = `${baseUrl}${req.params[0]}`
  const request = superagent[method](url)

  if (req.query) {
    request.query(_.omit(req.query, [config.apiKeyField]))
  }

  if (req.body) {
    request.send(req.body)
  }

  request.end((error, resourcesResponse) => {
    if (error) {
        return res.status(error.status).send(resourcesResponse)
    }

    return res.json(resourcesResponse.body)
  })
}
