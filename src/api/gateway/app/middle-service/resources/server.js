'use strict'

require(`${process.cwd()}/app/lib/globals`)

const App = $require('initializers/app')
const config = $require('lib/config')('services').resources
const services = $require('middlewares/services')
const middleWares = [
  require('./helpers/body-parser'),
  require('./handlers')
]

const serverStart = new App()
  .config(config)
  .applyMiddleWares(middleWares)
  .start()

module.exports = serverStart
  .tap(server => {
    let address = server.address()
    console.log(`Middle service "resources" started at address: 127.0.0.1:${address.port}`)
  })
  .catch(err => {
    console.log('Server start error:', err)
  })
