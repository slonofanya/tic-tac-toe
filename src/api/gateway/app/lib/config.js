'use strict'

const twFactorConfig = require('12factor-config')
const prefix = 'WEB_APPS'

function passConfigTree(configs) {
  return Object.keys(configs || {}).reduce((result, key) => {
    let item = configs[key]

    if (_.isString(item) && item && JSON.test(item)) {
      item = passConfigTree(JSON.parse(item))
    }

    result[key] = item

    return result
  }, {})
}

module.exports = key => {
  const constants = require(`${process.cwd()}/config/${key}.json`) || {}
  const configs = Object.keys(constants).reduce((result, key) => {
    result[key] = {
      env: `${prefix}_${key.toUpperCase()}`,
      default: constants[key] || undefined
    }

    return result
  }, {})

  // console.log(configs)
  return passConfigTree(twFactorConfig(configs))
}
