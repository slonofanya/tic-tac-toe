'use strict'

module.exports = {
  forbidden: new Error('You haven\'t enough permissions to do this operation'),
  unhandled: new Error('Unhandled error')
}
