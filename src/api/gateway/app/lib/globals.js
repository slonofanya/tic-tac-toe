'use strict'

const path = require('path')
const fse = require('fs-extra')
const Promise = require('bluebird')
const _ = require('lodash')
const exceptions = require('./exceptions')

const appPath = `${process.cwd()}/app`

const startsFrom = startsPath => new RegExp(`${startsPath}*`)

function resolvePath () {
  return path.resolve.apply(null, [appPath].concat(...arguments))
}

function $require (incPath) {
  return require(`${appPath}/${incPath}`)
}

Promise.promisifyAll(fse)

JSON.test = str => {
  try {
    const resultStr = JSON.parse(str)
    return _.isArray(resultStr) || _.isPlainObject(resultStr)
  } catch ( ex ) {
    return false
  }
}

function getException(kind, status) {
  const exception = _.get(exceptions, kind, _.get(exceptions, 'unhandled'))
  exception.status = status || 500
  return exception
}

Object.assign(global, {
  $require, Promise, resolvePath, _, startsFrom, getException
})
