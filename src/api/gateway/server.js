'use strict'

require('./app/lib/globals')

const App = $require('initializers/app')
const config = $require('lib/config')('server')
const logger = $require('middlewares/logger')
const services = $require('middlewares/services')
const middleWares = [
  $require('middlewares/auth')
]

const serverStart = App.getInstance()
  .config(config)
  .logger(logger)
  .applyMiddleWares(middleWares)
  .services(services)
  .start()

module.exports = serverStart
  .tap(server => {
    let address = server.address()
    console.log(`Server API started at address: 127.0.0.1:${address.port}`)
  })
  .catch(err => {
    console.log('Server start error:', err)
  })
