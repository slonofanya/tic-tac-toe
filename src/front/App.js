import React, { Component } from 'react'
import login from './containers/Login'
import Chat from './containers/Chat'
import Board from './containers/Board'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <nav className='navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse'>
          <a className='navbar-brand' href='#'>TicTacToe</a>

          <div className='collapse navbar-collapse' id='navbarsExampleDefault'>
            <ul className='navbar-nav mr-auto'>
              <li className='nav-item active'>
                <img src='logo.svg' className='App-logo' alt='logo'/>
              </li>
            </ul>

            <form className='form-inline my-2 my-lg-0'>
              <input className='form-control mr-sm-2' type='text' placeholder='Username'/>
              <button className='btn btn-outline-success my-2 my-sm-0' type='submit'>Sign in</button>
            </form>
          </div>
        </nav>

        <login />

        <div className='row'>
          <div className='col-md-8'>
            <Board />
          </div>

          <div className='col-md-4'>
            <Chat />
          </div>
        </div>
      </div>
    )
  }
}

export default App
