import React, { Component } from 'react'
import TicTacToe from '../components/TicTacToe'

export default class Board extends Component {
  render () {
    return (
      <div className='board'>
        <TicTacToe />
      </div>
    )
  }
}
