import React, { Component } from 'react'

export default class Chat extends Component {

  componentDidMount () {
    window.socket = window.io()

    window.$('.chat-form').submit(() => {
      window.socket.emit('chat message',  window.$('#m').val())
      window.$('#m').val('')
      return false
    })

    window.socket.on('chat message', (msg) => {
      window.$('.messages').append( window.$('<li>').text(msg))
    })
  }

  render () {
    return (
      <div className='chat'>
        <h4>Chat</h4>

        <ul className='messages' />

        <form className='chat-form'>
          Message:
          <input id='m'/>
        </form>
      </div>
    )
  }
}
