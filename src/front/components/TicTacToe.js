import React from 'react'
import _ from 'lodash'
const t = new Array(9);

function player2 () {
  const id = Math.floor(Math.random() * 9)
  t[id] ? player2() : move(id, 'player2')
}

function checkEnd () {
  if (t[0] == 'player2' && t[1] == 'player2' && t[2] == 'player2' || t[0] == 'player1' && t[1] == 'player1' && t[2] == 'player1')  return true
  if (t[3] == 'player2' && t[4] == 'player2' && t[5] == 'player2' || t[3] == 'player1' && t[4] == 'player1' && t[5] == 'player1')  return true
  if (t[6] == 'player2' && t[7] == 'player2' && t[8] == 'player2' || t[6] == 'player1' && t[7] == 'player1' && t[8] == 'player1')  return true
  if (t[0] == 'player2' && t[3] == 'player2' && t[6] == 'player2' || t[0] == 'player1' && t[3] == 'player1' && t[6] == 'player1')  return true
  if (t[1] == 'player2' && t[4] == 'player2' && t[7] == 'player2' || t[1] == 'player1' && t[4] == 'player1' && t[7] == 'player1')  return true
  if (t[2] == 'player2' && t[5] == 'player2' && t[8] == 'player2' || t[2] == 'player1' && t[5] == 'player1' && t[8] == 'player1')  return true
  if (t[0] == 'player2' && t[4] == 'player2' && t[8] == 'player2' || t[0] == 'player1' && t[4] == 'player1' && t[8] == 'player1')  return true
  if (t[2] == 'player2' && t[4] == 'player2' && t[6] == 'player2' || t[2] == 'player1' && t[4] == 'player1' && t[6] == 'player1')  return true
  if (t[0] && t[1] && t[2] && t[3] && t[4] && t[5] && t[6] && t[7] && t[8]) return true
}

function move (id, role) {
  if (t[id]) return false
  t[id] = role
  document.getElementById(id).className = 'cell ' + role
  !checkEnd() ? (role == 'player1') ? player2() : null : reset()
}

function reset () {
  alert('Игра окончена!')
  location.reload()
}

export default () => (
    <div className='table'>
      {_.map(_.times(9), i => (
        <div
          key={`cell-${i}`}
          className='cell'
          id={i}
          onClick={move.bind(null, i, 'player1')}>
        </div>
      ))}
    </div>
)
