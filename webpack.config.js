const webpack = require('webpack')
const { resolve } = require('path')

const contentPath = resolve(__dirname, 'public/dist')

const config = {
  devtool: 'cheap-eval-source-map',
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client',
    'webpack/hot/only-dev-server',
    './client.js'
  ],
  output: {
    path: contentPath,
    publicPath: '/dist/',
    filename: 'index.js'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/,
        include: __dirname
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ]
}

module.exports = config
