const express = require('express')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const webpack = require('webpack')

const { port } = require('./config')
const webpackConfig = require('./webpack.config')

const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)
const compiler = webpack(webpackConfig)

app.use(express.static(__dirname + '/public'))

app.use(webpackDevMiddleware(compiler, {
  noInfo: false,
  publicPath: webpackConfig.output.publicPath,
  hot: true
}))
app.use(webpackHotMiddleware(compiler))

io.on('connection', socket => {
  socket.on('chat message', msg => {
    console.log('chat message', msg)
    io.emit('chat message', msg)
  })
})

http.listen(port, () => console.log(`listening on *:${port}`))
